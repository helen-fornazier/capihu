#ifndef DOOR_BUTTON_WALL_OBJECTS_HEADER
#define DOOR_BUTTON_WALL_OBJECTS_HEADER

#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define DOOR_BUTTON_WALL_VISUAL1 '='
#define DOOR_BUTTON_WALL_VISUAL2 '+'
#define DOOR_BUTTON_WALL_COLOUR1 WHT
#define DOOR_BUTTON_WALL_COLOUR2 CYN

enum door_button_wall_state
{
    DOOR_BUTTON_WALL_CLOSED,
    DOOR_BUTTON_WALL_OPEN
};

/* door_button_wall objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update. */
struct door_button_wall
{
    struct object o;
    int pos_x,pos_y;
    char init;
    enum door_button_wall_state state;
};

void door_button_wall_unlock();
struct door_button_wall door_button_wall_init(int pos_x, int pos_y);

#endif
