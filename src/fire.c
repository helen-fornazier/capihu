#include "fire.h"
#include "wall.h"

struct fire g_fire;

/* Function update decides whether the object *o is to be placed on the stage.
 * When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.
 * time is used to decide which animation frame is used for drawing the fire.*/
static int
_fire_update(struct object *o, t_stage st)
{
	struct fire *f = (struct fire*)o;
    struct timespec time;
    long ntime;
    long diff;

    if(f->init == 0)
    {
        if(f->ori == HORIZONTAL)
            for (int i = f->pos_start; i < f->pos_end; i++)
                st[f->pos_fix][i] = O_FIRE;
        else
            for (int i = f->pos_start; i < f->pos_end; i++)
                st[i][f->pos_fix] = O_FIRE;
        f->init = 1;
    }

    timespec_get(&time, TIME_UTC); //gets time in nanoses
    ntime = time.tv_nsec / 10000000;
    diff = ntime % VISUAL_CYCLE_TIME;

    if(diff == 0 && f->already_moved_this_cycle == 0)
    {
        f->already_moved_this_cycle = 1;
        if(f->visual != FIRE_VISUAL1)
        {
            f->visual = FIRE_VISUAL1;
            return 1;
        }
        else
        {
            f->visual = FIRE_VISUAL2;
            return 1;
        }
    }
    if(diff > 0)
    {
        f->already_moved_this_cycle = 0;
        return 0;
    }
    return 0;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _fire_draw(struct object *o, struct screen *s)
{
    struct fire *f = (struct fire*)o;
    if (f->ori == HORIZONTAL)
    {
        for (int i = f->pos_start; i < f->pos_end; i++)
        {
            s->c[f->pos_fix][i] = FIRE_COLOUR;
            s->m[f->pos_fix][i] = f->visual;
        }
    }
    else
    {
        for (int i = f->pos_start; i < f->pos_end; i++)
        {
            s->c[i][f->pos_fix] = FIRE_COLOUR;
            s->m[i][f->pos_fix] = f->visual;
        }
    }
}

/* This function erases the object *o from stage.*/
static void _fire_destroy(struct object *o, t_stage st)
{
    struct fire *f = (struct fire*)o;
    if (!f->init) return;
    if (f->ori == HORIZONTAL)
        for (int i = f->pos_start; i < f->pos_end; i++)
            st[f->pos_fix][i] = O_EMPTY;
    else
        for (int i = f->pos_start; i < f->pos_end; i++)
            st[i][f->pos_fix] = O_EMPTY;
    f->init = 0;
}

/* Object constructor */
struct fire fire_init(enum w_orientation ori, int pos_fix,
          int pos_start, int pos_end)
{
    struct fire f;
    f.o.update = _fire_update;
    f.o.draw = _fire_draw;
    f.o.destroy = _fire_destroy;
    f.ori = ori;
    f.pos_fix = pos_fix;
    f.pos_start = pos_start;
    f.pos_end = pos_end;
    f.visual = FIRE_VISUAL1;
    f.already_moved_this_cycle = 0;
    return f;
}
