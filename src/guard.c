#include "guard.h"
#include "key.h"

struct guard g_guard;

#define _STUN_ID_RANGE 16

static void
_set_sight(struct guard *g, t_stage st, int clear)
{
    int d_mult = (g->d == UP ? -1 : 1);
    enum obj_id
        id_set,
        id_old;

    if (clear)
    {
        id_set = O_EMPTY;
        id_old = O_SIGHT;
    }
    else
    {
        id_set = O_SIGHT;
        id_old = O_EMPTY;
    }
    for (int i = 0; i < GUARD_SIGHT_DISTANCE; i++)
    {
        if (st[g->pos_y + (d_mult * (i + 2))][g->pos_x] == id_old)
            st[g->pos_y + (d_mult * (i + 2))][g->pos_x] = id_set;
        for (int j = 0; j <= i; j++)
        {
            if (st[g->pos_y + (d_mult * (i + 2))][g->pos_x + j + 1]
                == id_old)
                st[g->pos_y + (d_mult * (i + 2))][g->pos_x + j + 1]
                = id_set;
            if (st[g->pos_y + (d_mult * (i + 2))][g->pos_x - j - 1]
                == id_old)
                st[g->pos_y + (d_mult * (i + 2))][g->pos_x - j - 1]
                = id_set;
        }
    }
}

static int
_stun_pos_get(t_time diff)
{
    return ((diff / GUARD_STUN_CYCLE)) % _STUN_ID_RANGE;
}

static void
_drop_key(struct guard *g, t_stage st)
{
    key_drop(g->pos_x - 3, g->pos_y, st);
}

static void
_guard_wake(struct guard *g, t_stage st)
{
    g->s = NOT_STUNNED;
    _set_sight(g, st, 0);
}

static int
_guard_stun(struct guard *g, t_stage st)
{
    t_time time, diff;
    int stun_pos;

    if (g->s == NOT_STUNNED) return 0;
    if (g->s == JUST_STUNNED)
    {
        g->s = STUNNED;
        g->stun_start = time_get_ms();
        _set_sight(g, st, 1);
        g->stun_pos = 0;
        if (g->has_key)
        {
            g->has_key = 0;
            _drop_key(g, st);
        }
        return 1;
    }
    time = time_get_ms();
    diff = time - g->stun_start;
    if (diff >= GUARD_STUN_DURATION)
    {
        _guard_wake(g, st);
        return 1;
    }
    stun_pos = _stun_pos_get(diff);
    if (stun_pos != g->stun_pos)
    {
        g->stun_pos = stun_pos;
        return 1;
    }
    return 0;
}

static void
_guard_stage(struct guard *g, t_stage st)
{
    for (int i = g->pos_y - 1; i <= g->pos_y + 1; i++)
    {
        for (int j = g->pos_x - 1; j <= g->pos_x + 1; j++)
        {
            st[i][j] = O_GUARD;
        }
    }
}

static int
_guard_update(struct object *o, t_stage st)
{
    struct guard *g = (struct guard*)o;
    if (!g->init)
    {
        _guard_stage(g, st);
        _guard_wake(g, st);
        g->init = 1;
    }
    return _guard_stun(g, st);
}

static void
_guard_only_draw(struct guard *g, struct screen *s)
{
    for (int i = g->pos_y - 1; i <= g->pos_y + 1; i++)
    {
        for (int j = g->pos_x - 1; j <= g->pos_x + 1; j++)
        {
            s->c[i][j] = GUARD_COLOUR;
            s->m[i][j] = GUARD_VISUAL;
        }
    }
}

static const int _stun_pos[_STUN_ID_RANGE][2] =
{
    { -2, -2 }, //1
    { -2, -1 }, //2
    { -2,  0 }, //3
    { -2,  1 }, //4
    { -2,  2 }, //5
    { -1,  2 }, //6
    {  0,  2 }, //7
    {  1,  2 }, //8
    {  2,  2 }, //9
    {  2,  1 }, //10
    {  2,  0 }, //11
    {  2, -1 }, //12
    {  2, -2 }, //13
    {  1, -2 }, //14
    {  0, -2 }, //15
    { -1, -2 }, //16
};

static void
_stun_draw(struct guard *g, struct screen *s)
{
    s->c[g->pos_y + _stun_pos[g->stun_pos][0]]
        [g->pos_x + _stun_pos[g->stun_pos][1]]
    = GUARD_STUN_COLOUR;
    s->m[g->pos_y + _stun_pos[g->stun_pos][0]]
        [g->pos_x + _stun_pos[g->stun_pos][1]]
    = GUARD_STUN_VISUAL;
}

static void
_sight_draw(struct guard *g, struct screen *s)
{
    int d_mult = (g->d == UP ? -1 : 1);
    char left, right;
    if (g->d == UP)
    {
        d_mult = -1;
        left = '\\';
        right = '/';
    }
    else
    {
        d_mult = 1;
        left = '/';
        right = '\\';
    }

    for (int i = 0; i < GUARD_SIGHT_DISTANCE; i++)
    {
        if (s->m[g->pos_y + (d_mult * (i + 2))][g->pos_x + i + 1]
            == ' ')
        {
            s->c[g->pos_y + (d_mult * (i + 2))][g->pos_x + i + 1]
            = GUARD_SIGHT_COLOUR;
            s->m[g->pos_y + (d_mult * (i + 2))][g->pos_x + i + 1]
            = right;
        }
        if (s->m[g->pos_y + (d_mult * (i + 2))][g->pos_x - i - 1]
            == ' ')
        {
            s->c[g->pos_y + (d_mult * (i + 2))][g->pos_x - i - 1]
            = GUARD_SIGHT_COLOUR;
            s->m[g->pos_y + (d_mult * (i + 2))][g->pos_x - i - 1]
            = left;
        }
    }
}

static void
_guard_draw(struct object *o, struct screen *s)
{
    struct guard *g = (struct guard*)o;
    _guard_only_draw(g, s);
    if (g->s != NOT_STUNNED) _stun_draw(g, s);
    else _sight_draw(g, s);
}

void
guard_stun(void)
{
    g_guard.s = JUST_STUNNED;
}

void
_guard_destroy(struct object *o, t_stage st)
{
    struct guard *g = (struct guard*)o;
    _set_sight(g, st, 1);
    for (int i = g->pos_y - 1; i <= g->pos_y + 1; i++)
    {
        for (int j = g->pos_x - 1; j <= g->pos_x + 1; j++)
        {
            st[i][j] = O_EMPTY;
        }
    }
}

void
guard_init(int pos_x, int pos_y, enum facing d, char has_key)
{
    g_guard.o.draw = _guard_draw;
    g_guard.o.update = _guard_update;
    g_guard.o.destroy = _guard_destroy;
    g_guard.pos_y = pos_y;
    g_guard.pos_x = pos_x;
    g_guard.stun_pos = 0;
    g_guard.d = d;
    g_guard.has_key = has_key;
    g_guard.s = NOT_STUNNED;
}

/* guard.h END */
