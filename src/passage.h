#ifndef PASSAGE_OBJECTS_HEADER
#define PASSAGE_OBJECTS_HEADER

#include "object.h"

#define PASSAGE_VISUAL ' '

/* create the wall objects */

enum passage_orientation
{
    HORIZONTAL,
    VERTICAL,
};

/* passage objects.
 * orientation determines if it is vertical or horizontal.
 * if vertical, pos_fix is the COLUMN and pos_start and end
 * are the start and end of the wall LINES.
 * if horizontal swap COLUMN for lines */
struct passage
{
    struct object o;
    enum passage_orientation ori;
    int
        pos_fix,
        pos_start,
        pos_end;
    char init;
};

struct passage wall_init(enum passage_orientation ori, int pos_fix,
          int pos_start, int pos_end);

#endif
