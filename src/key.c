#include "key.h"
#include "door_key.h"
#include "players.h"

extern struct player g_thief;

struct key g_key;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int _key_update(struct object *o, t_stage st)
{
    struct key *key = (struct key*)o;
    if (key->dropped && !key->grabbed)
        st[key->pos_y][key->pos_x] = O_KEY;
    if (key->updated)
    {
        key->updated--;
        return 1;
    }
    return 0;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void
_key_draw(struct object *o, struct screen *s)
{
    struct key *key = (struct key*)o;

    if (key->dropped && !key->grabbed)
    {
        s->c[key->pos_y][key->pos_x] = KEY_COLOUR;
        s->m[key->pos_y][key->pos_x] = KEY_VISUAL;
    }
    if (key->grabbed)
    {
        s->c[g_thief.pos_y][g_thief.pos_x] = KEY_COLOUR;
        s->m[g_thief.pos_y][g_thief.pos_x] = KEY_VISUAL;
    }
}

/* This function erases the object *o from stage.*/
static void
_key_destroy(struct object *o, t_stage st)
{
    struct key *key_ptr = (struct key*)o;
    st[key_ptr->pos_y][key_ptr->pos_x] = O_EMPTY;
}

/* This function is used to "glue" the key to the thief. */
void key_grab(t_stage st)
{
    st[g_key.pos_y][g_key.pos_x] = O_EMPTY;
    g_key.grabbed = 1;
}

/* Function used by the thief to define position of key when released by the thief. */
void key_drop(int pos_x, int pos_y, t_stage st)
{
    g_key.pos_x = pos_x;
    g_key.pos_y = pos_y;
    g_key.dropped = 1;
    g_key.updated = 1;
    if (st[g_key.pos_y][g_key.pos_x] == O_THIEF)
        g_key.grabbed = 1;
    else if (st[g_key.pos_y][g_key.pos_x] == O_DOOR_KEY)
    {
        g_key.dropped = 0;
        door_key_unlock();
    }
    else if (st[g_key.pos_y][g_key.pos_x] != O_EMPTY) return;
    st[g_key.pos_y][g_key.pos_x] = O_KEY;
    g_key.grabbed = 0;
}

/* Object constructor */
void key_init(void)
{
    g_key.o.update = _key_update;
    g_key.o.draw = _key_draw;
    g_key.o.destroy = _key_destroy;
    g_key.dropped = 0;
    g_key.grabbed = 0;
}
