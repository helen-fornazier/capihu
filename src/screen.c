#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "screen.h"
#include <unistd.h>

int usleep(useconds_t usec);

#define NRM_CMD  "\x1B[0m"  //This color resets everything to default
#define BLK_CMD  "\x1B[30m"
#define RED_CMD  "\x1B[31m"
#define GRN_CMD  "\x1B[32m"
#define YEL_CMD  "\x1B[33m"
#define BLU_CMD  "\x1B[34m"
#define MAG_CMD  "\x1B[35m"
#define CYN_CMD  "\x1B[36m"
#define WHT_CMD  "\x1B[37m"
#define B_BLK_CMD  "\x1B[90m"
#define B_RED_CMD  "\x1B[91m"
#define B_GRN_CMD  "\x1B[92m"
#define B_YEL_CMD  "\x1B[93m"
#define B_BLU_CMD  "\x1B[94m"
#define B_MAG_CMD  "\x1B[95m"
#define B_CYN_CMD  "\x1B[96m"
#define B_WHT_CMD  "\x1B[97m"
#define GRY_CMD  "\x1B[3m"
#define BLINKING_B_RED_CMD  "\x1B[5m\x1B[91m"
#define BLINKING_B_GRN_CMD  "\x1B[5m\x1B[92m"
#define BLINKING_B_YEL_CMD  "\x1B[5m\x1B[93m"
#define BLINKING_B_BLU_CMD  "\x1B[5m\x1B[94m"
#define BLINKING_B_MAG_CMD  "\x1B[5m\x1B[95m"
#define BLINKING_B_CYN_CMD  "\x1B[5m\x1B[96m"
#define BLINKING_B_WHT_CMD  "\x1B[5m\x1B[97m"

static char *_colours[] =
{
    [NRM] = NRM_CMD,
    [BLK] = BLK_CMD,
    [RED] = RED_CMD,
    [GRN] = GRN_CMD,
    [YEL] = YEL_CMD,
    [BLU] = BLU_CMD,
    [MAG] = MAG_CMD,
    [CYN] = CYN_CMD,
    [WHT] = WHT_CMD,
    [B_BLK] = B_BLK_CMD,
    [B_RED] = B_RED_CMD,
    [B_GRN] = B_GRN_CMD,
    [B_YEL] = B_YEL_CMD,
    [B_BLU] = B_BLU_CMD,
    [B_MAG] = B_MAG_CMD,
    [B_CYN] = B_CYN_CMD,
    [B_WHT] = B_WHT_CMD,
    [GRY] = GRY_CMD,
    [BLINKING_B_RED] = BLINKING_B_RED_CMD,
    [BLINKING_B_GRN] = BLINKING_B_GRN_CMD,
    [BLINKING_B_YEL] = BLINKING_B_YEL_CMD,
    [BLINKING_B_BLU] = BLINKING_B_BLU_CMD,
    [BLINKING_B_MAG] = BLINKING_B_MAG_CMD,
    [BLINKING_B_CYN] = BLINKING_B_CYN_CMD,
    [BLINKING_B_WHT] = BLINKING_B_WHT_CMD,
};

static void
_char_colour_print(enum colour co, char ch)
{

    printf("%s%c%s", _colours[co], ch, NRM_CMD);    //NRM_CMD to force reset color
}

void
screen_draw(struct screen *s)
{
    printf("\033[H\033[J");
    for (int i = 0; i < s->lin; i++)
    {
        for (int j = 0; j < s->col; j++)
            _char_colour_print(s->c[i][j], s->m[i][j]);
        printf("\n");
    }
    printf("W A S D Q E\n");
}

void
screen_clear(struct screen *s)
{
    memset(s->c, NRM, sizeof(s->c));
    memset(s->m, ' ', sizeof(s->m));
}

void
screen_set_dimension(struct screen *s, int l, int c)
{
    s->lin = l;
    s->col = c;
    if ((l > S_LINE) || (c >= S_COLUMN))
    {
        printf("%sERROR: screen dimensions are too big\n",
               _colours[RED]);
        exit(1);
    }
}
