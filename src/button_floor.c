#include "button_floor.h"
#include "door_button_floor.h"

struct button_floor g_button_floor;
extern struct door_button_floor g_door_button_floor;
/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int _button_floor_update(struct object *o, t_stage st)
{
    struct button_floor *button_floor_ptr = (struct button_floor*)o;

    if(button_floor_ptr->init == 0)
    {
        button_floor_ptr->state = DOOR_BUTTON_FLOOR_OFF;
        st[button_floor_ptr->pos_y][button_floor_ptr->pos_x] = O_BUTTON_FLOOR;
        button_floor_ptr->init = 1;
        return 1;
    }
    else if(st[button_floor_ptr->pos_y][button_floor_ptr->pos_x] != O_BUTTON_FLOOR && button_floor_ptr->state == DOOR_BUTTON_FLOOR_OFF)
    {
        void door_button_floor_unlock();
        button_floor_ptr->state = DOOR_BUTTON_FLOOR_ON;
        return 1;
    }
    else if(DOOR_BUTTON_FLOOR_ON && st[button_floor_ptr->pos_y][button_floor_ptr->pos_x] == O_EMPTY)
    {
        void door_button_floor_relock();
        button_floor_ptr->state = DOOR_BUTTON_FLOOR_OFF;
        st[button_floor_ptr->pos_y][button_floor_ptr->pos_x] = O_BUTTON_FLOOR;
        return 1;
    }

    return 0;    
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _button_floor_draw(struct object *o, struct screen *s)
{   
    struct button_floor *button_floor_ptr = (struct button_floor*)o;
    
    if(button_floor_ptr->state == DOOR_BUTTON_FLOOR_OFF)
    {
        s->c[button_floor_ptr->pos_y][button_floor_ptr->pos_x] = BUTTON_FLOOR_COLOUR;
        s->m[button_floor_ptr->pos_y][button_floor_ptr->pos_x] = BUTTON_FLOOR_VISUAL;
        return;
    }
    return;
}

/* This function erases the object *o from stage.*/
static void _button_floor_destroy(struct object *o, t_stage st)
{
    struct button_floor *button_floor_ptr = (struct button_floor*)o;
    if (!button_floor_ptr->init) return;
        st[button_floor_ptr->pos_y][button_floor_ptr->pos_x] = O_EMPTY;
    button_floor_ptr->init = 0;
}

/* Object constructor */
struct button_floor button_floor_init(int pos_x, int pos_y)
{
    struct button_floor b;
    b.o.update = _button_floor_update;
    b.o.draw = _button_floor_draw;
    b.o.destroy = _button_floor_destroy;
    b.pos_x = pos_x;
    b.pos_y = pos_y;
    b.init = 0;
    b.state = DOOR_BUTTON_FLOOR_OFF;
    return b;
}
