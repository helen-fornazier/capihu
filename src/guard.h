#ifndef GUARD_OBJECT_HEADER
#define GUARD_OBJECT_HEADER

#include "capihu_time.h"
#include "object.h"

#define GUARD_STUN_CYCLE 200
#define GUARD_STUN_DURATION 8000
#define GUARD_SIGHT_DISTANCE 5

#define GUARD_SIGHT_COLOUR YEL

#define GUARD_COLOUR GRN
#define GUARD_VISUAL 'G'

#define GUARD_STUN_COLOUR NRM
#define GUARD_STUN_VISUAL '*'

enum stunned
{
    NOT_STUNNED,
    JUST_STUNNED,
    STUNNED
};

struct guard
{
    struct object o;
    int
        pos_y, // line of the center of the guard position
        pos_x, // column of the center of the guard position
        stun_pos; // position "id" of the stun star
    enum facing d; // direction the guard is facing
    char
        has_key, // if the guard has a key to drop
        init; // if the first update already happened
    enum stunned s; // state of stun of the guard
    t_time stun_start; // time the stun started
};

void
guard_stun(void);

/* facing d must be either up or down */
void
guard_init(int pos_s, int pos_y, enum facing d, char has_key);

#endif
