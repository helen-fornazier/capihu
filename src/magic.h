#ifndef MAGIC_OBJECTS_HEADER
#define MAGIC_OBJECTS_HEADER

#include <time.h>
#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define MAGIC_VISUAL '@'
#define MAGIC_COLOUR B_MAG
#define MAGIC_MOV_UPDATE_TIME 10	//the higher the value the slower the magic moves

enum magic_state
{
    MAGIC_UP,
    MAGIC_DOWN,
    MAGIC_LEFT,
    MAGIC_RIGHT,
    MAGIC_NOT_EXISTING
};

/* magic objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update.
 * magic_state controls wheather the magic is moving and its direction.
 * already_moved_this_cycle is a flag to control its speed moviment.*/
struct magic
{
    struct object o;
    int pos_x, pos_y;
    char init;
    enum magic_state state;
    int already_moved_this_cycle;
    int grabbed;
};


struct magic magic_init(int pos_x, int pos_y, enum magic_state state);
void change_magic_state_and_grab_state(enum magic_state state,int grabbed, int pos_x, int pos_y);

#endif
