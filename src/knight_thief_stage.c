#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "input_handler.h"
#include "capihu_time.h"
#include "string.h"
#include "players.h"
#include "magic.h"
#include "attack.h"
#include "door_key.h"
#include "stone.h"
#include "button_wall.h"
#include "door_button_wall.h"

extern struct key g_key;
extern struct player g_thief;
extern struct player g_knight;
extern struct attack g_attack;
extern struct door_key g_door_key;
extern struct stone g_stone;
extern struct button_wall g_button_wall;
extern struct door_button_wall g_door_button_wall;
extern struct guard g_guard;

int
main(void)
{
    struct wall w1, w2, w3, w4, w5, w6, w7;
    struct object *array[10];
    t_stage st;
    struct screen s;
    char update;

    memset(st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    w1 = wall_init(HORIZONTAL, 2, 2, 19);
    w2 = wall_init(VERTICAL, 2, 2, 16);
    w3 = wall_init(VERTICAL, 22, 2, 16);
    w4 = wall_init(HORIZONTAL, 10, 2, 11);
    w5 = wall_init(HORIZONTAL, 10, 14, 19);
    w6 = wall_init(HORIZONTAL, 10, 20, 22);
    w7 = wall_init(HORIZONTAL, 15, 6, 22);
    key_init();
    thief_init(4, 4, st);
    knight_init(19, 12, st);
    attack_init();
    guard_init(12, 9, UP, 1);
    g_door_key = door_key_init(20, 2);
    g_stone = stone_init(8, 8);
    g_button_wall = button_wall_init(19, 14);
    g_door_button_wall = door_button_wall_init(4, 15);

    screen_set_dimension(&s, 24, 24);

    array[0] = &w1.o;
    array[1] = &w2.o;
    array[2] = &w3.o;
    array[3] = &w4.o;
    array[4] = &w5.o;
    array[5] = &w6.o;
    array[6] = &w7.o;
    array[7] = &g_thief.o;
    array[8] = &g_key.o;
    array[9] = &g_stone.o;
    array[10] = &g_door_button_wall.o;
    array[11] = &g_door_key.o;
    array[12] = &g_button_wall.o;
    array[13] = &g_guard.o;
    array[14] = &g_knight.o;
    array[15] = &g_attack.o;

    set_players(1, 0, 1);
    while(1)
    {
        update = 0;
        if (!get_player_input(st)) return 0;
        for (int i = 0; i < 16; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 16; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
        if ((g_thief.pos_y <= 2))
            g_thief.pos_y = 3;
        if ((g_knight.pos_y >= 15))
            g_knight.pos_y = 14;
        if (g_thief.pos_y == 3 && g_knight.pos_y == 14) return 0;
    }
}
