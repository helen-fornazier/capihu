#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "input_handler.h"
#include "capihu_time.h"
#include "string.h"
#include "players.h"
#include "magic.h"
#include "door_button_floor.h"
#include "button_floor.h"
#include "door_button_wall.h"
#include "button_wall.h"
#include "fire.h"

extern struct guard g_guard;
extern struct key g_key;

extern struct player g_knight;
extern struct player g_mage;
extern struct player g_thief;
extern struct magic g_magic;
extern struct door_button_floor g_door_button_floor;
extern struct button_floor g_button_floor;
extern struct door_button_wall g_door_button_wall;
extern struct button_wall g_button_wall;

int
main(void)
{
    struct wall w1, w2, w3, w4;//,w5,w6,w7;
    struct fire f1;//, f2;
    struct object *array[14];
    t_stage st;
    struct screen s;
    char update;

    memset(st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    w1 = wall_init(HORIZONTAL, 7, 6, 30);
    f1 = fire_init(HORIZONTAL, 11, 6, 26);
    w2 = wall_init(VERTICAL, 6, 12, 20);
    w3 = wall_init(VERTICAL, 29, 7, 20);
    w4 = wall_init(HORIZONTAL, 20, 6, 30);
    g_door_button_wall = door_button_wall_init(27,11);
    g_button_wall = button_wall_init(10,8);

    mage_init(16, 16, st);
    g_magic = magic_init(0,0,MAGIC_NOT_EXISTING);
    

    screen_set_dimension(&s, 24, 40);

    array[0] = &w1.o;
    array[1] = &f1.o;
    array[2] = &w3.o;
    array[3] = &w2.o;
    array[4] = &w4.o;
    array[5] = &g_mage.o;
    array[6] = &g_magic.o;
    array[7] = &g_door_button_wall.o;
    array[8] = &g_button_wall.o;

    set_players(0, 1, 0);
    while(1)
    {
        update = 0;
        if (!get_player_input(st)) return 0;
        for (int i = 0; i < 9; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 9; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
        if ((g_mage.pos_x == 6) && (g_mage.pos_y == 9))
            return 0;
    }
}
