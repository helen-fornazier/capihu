#include "input_handler.h"
#include "input_thread.h"
#include "players.h"
#include "guard.h"

static enum players _p_enabled;

static char _p_available[P_COUNT];

extern struct player g_knight;
extern struct player g_mage;
extern struct player g_thief;

static struct player *_players[P_COUNT] =
{
    [KNIGHT] = &g_knight,
    [MAGE] = &g_mage,
    [THIEF] = &g_thief
};

static void (*_special[P_COUNT])(t_stage st) =
{
    [KNIGHT] = knight_special,
    [MAGE] = mage_special,
    [THIEF] = thief_special
};


static void
_player_rotate(void)
{
    int i;
    _players[_p_enabled]->selected = 0;
    for (i = 1; i < P_COUNT + 1; i++)
    {
        if (_p_available[(_p_enabled + i) % P_COUNT])
            break;
    }
    _p_enabled = (_p_enabled + i) % P_COUNT;
    _players[_p_enabled]->selected = 1;
}

void
set_players(char knight, char mage, char thief)
{
    _p_available[KNIGHT] = knight;
    _p_available[MAGE] = mage;
    _p_available[THIEF] = thief;
    if (knight)
    {
        _p_enabled = KNIGHT;
        g_knight.selected = 1;
    }
    else if(mage)
    {
        _p_enabled = MAGE;
        g_mage.selected = 1;
    }
    else
    {
        _p_enabled = THIEF;
        g_thief.selected = 1;
    }
}

int
get_player_input(t_stage st)
{
    char input = get_input();
    switch (input)
    {
        case 'l':
            return 0;
        case 'w':
            _players[_p_enabled]->dir = UP;
            _players[_p_enabled]->input = MOVE;
            break;
        case 'a':
            _players[_p_enabled]->dir = LEFT;
            _players[_p_enabled]->input = MOVE;
            break;
        case 's':
            _players[_p_enabled]->dir = DOWN;
            _players[_p_enabled]->input = MOVE;
            break;
        case 'd':
            _players[_p_enabled]->dir = RIGHT;
            _players[_p_enabled]->input = MOVE;
            break;
        case 'q':
            _special[_p_enabled](st);
            break;
        case 'e':
            _player_rotate();
            break;
        case 'P':
            guard_stun();
            break;
        default:
            _players[_p_enabled]->input = IDLE;
            break;
    }
    return 1;
}
