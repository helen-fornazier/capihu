#ifndef OBJECT_INTERFACE_HEADER
#define OBJECT_INTERFACE_HEADER

#include "screen.h"

/* the stage will be used to determine colisions.
 * each object will put its identifier at the positions it
 * occupies, so whenever there is an update, the object can check
 * for issues.
 * 0 is reserved for free space */

enum obj_id
{
    O_EMPTY = 0,            //done
    O_WALL,                 //done
    O_SIGHT,
    O_GUARD,
    O_KEY,
    O_THIEF,
    O_STONE,                //done
    O_MAGE,
    O_MAGIC,
    O_KNIGHT,
    O_BASH,
    O_DOOR_KEY,
    O_DOOR_BUTTON_FLOOR,
    O_DOOR_BUTTON_WALL,
    O_BOMB,                 //done
    O_BUTTON_FLOOR,
    O_BUTTON_WALL,
    O_PASSAGE,
    O_OBSTACLE,
    O_FIRE                  //done
};

typedef enum obj_id t_stage[S_LINE][S_COLUMN];

/* This is an interface of an object in the game.
 * All objects need to implement the function update, and draw and
 * destroy.
 * update will update the object state for a time unit. it returns
 * 1 if an update was done and 0 if nothing changed
 * draw, which will fill the screen with the object's elements.
 * detroy should remove the traces from the object and free its
 * resources. */

struct object
{
    int (*update)(struct object *o, t_stage st);
    void (*draw)(struct object *o, struct screen *s);
    void (*destroy)(struct object *o, t_stage st);
};

/* this enum create directions that will be used by some
 * objects to represent the direction they're facing */
enum facing
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

#endif
