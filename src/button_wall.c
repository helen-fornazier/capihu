#include "button_wall.h"
#include "door_button_wall.h"

struct button_wall g_button_wall;
extern struct door_button_wall g_door_button_wall;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int _button_wall_update(struct object *o, t_stage st)
{
    struct button_wall *button_wall_ptr = (struct button_wall*)o;

    if(button_wall_ptr->state == BUTTON_WALL_OFF)
    {
        st[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = O_BUTTON_WALL;
        if(button_wall_ptr->init == 0)
        {
            button_wall_ptr->init = 1;
            return 1;
        }
        else return 0;
    }
    else
    {
        st[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = O_EMPTY;
        door_button_wall_unlock();
        if(button_wall_ptr->init == 0)
        {
            button_wall_ptr->init = 1;
            return 1;
        }
        else return 0;
    }
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _button_wall_draw(struct object *o, struct screen *s)
{
    struct button_wall *button_wall_ptr = (struct button_wall*)o;
    if(button_wall_ptr->state == BUTTON_WALL_ON)
    {
    /*    s->c[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = NRM;
        s->m[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = 0;*/
        return;
    }

    s->c[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = BUTTON_WALL_COLOUR;
    s->m[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = BUTTON_WALL_VISUAL;
}

/* This function erases the object *o from stage.*/
static void _button_wall_destroy(struct object *o, t_stage st)
{
    struct button_wall *button_wall_ptr = (struct button_wall*)o;
    if (!button_wall_ptr->init) return;
    st[button_wall_ptr->pos_y][button_wall_ptr->pos_x] = O_EMPTY;
    button_wall_ptr->init = 0;
}

/* This function changes the state of the button to either OFF or ON by the stone object or
by the magic object.*/
void button_wall_change_state(enum button_wall_state state)
{
    g_button_wall.state = state;
    g_button_wall.init = 0;
    return;
}

/* Object constructor */
struct button_wall button_wall_init(int pos_x, int pos_y)
{
    struct button_wall b;
    b.o.update = _button_wall_update;
    b.o.draw = _button_wall_draw;
    b.o.destroy = _button_wall_destroy;
    b.pos_x = pos_x;
    b.pos_y = pos_y;
    b.init = 0;
    b.state = BUTTON_WALL_OFF;
    return b;
}
