#ifndef BUTTON_FLOOR_OBJECTS_HEADER
#define BUTTON_FLOOR_HEADER

#include <time.h>
#include "object.h"


/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define BUTTON_FLOOR_VISUAL 'O'
#define BUTTON_FLOOR_COLOUR RED

enum button_floor_state
{
    DOOR_BUTTON_FLOOR_ON,
    DOOR_BUTTON_FLOOR_OFF
};

/* button_floor objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update. */
struct button_floor
{
    struct object o;
    int pos_x, pos_y;
    char init;
    enum button_floor_state state;
};


struct button_floor button_floor_init(int pos_x, int pos_y);

#endif
