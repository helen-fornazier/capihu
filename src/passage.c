#include "passage.h"

struct passage g_passage;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int _passage_update(struct object *o, t_stage st)
{
    struct passage *passage_ptr = (struct passage*)o;
    if (passage_ptr->init) return 0;
    if (passage_ptr->ori == HORIZONTAL)
        for (int i = passage_ptr->pos_start; i < passage_ptr->pos_end; i++)
            st[passage_ptr->pos_fix][i] = O_PASSAGE;
    else
        for (int i = passage_ptr->pos_start; i < passage_ptr->pos_end; i++)
            st[i][passage_ptr->pos_fix] = O_PASSAGE;
    passage_ptr->init = 1;
    return 1;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _passage_draw(struct object *o, struct screen *s)
{
    struct passage *passage_ptr = (struct passage*)o;
    if (passage_ptr->ori == HORIZONTAL)
    {
        for (int i = passage_ptr->pos_start; i < passage_ptr->pos_end; i++)
        {
            s->c[passage_ptr->pos_fix][i] = PASSAGE_COLOUR;
            s->m[passage_ptr->pos_fix][i] = PASSAGE_VISUAL;
        }
    }
    else
    {
        for (int i = passage_ptr->pos_start; i < passage_ptr->pos_end; i++)
        {
            s->c[i][passage_ptr->pos_fix] = PASSAGE_COLOUR;
            s->m[i][passage_ptr->pos_fix] = PASSAGE_VISUAL;
        }
    }
}

/* This function erases the object *o from stage.*/
static void _passage_destroy(struct object *o, t_stage st)
{
    struct passage *passage_ptr = (struct passage*)o;
    if (!passage_ptr->init) return;
    if (passage_ptr->ori == HORIZONTAL)
        for (int i = passage_ptr->pos_start; i < passage_ptr->pos_end; i++)
            st[passage_ptr->pos_fix][i] = O_EMPTY;
    else
        for (int i = passage_ptr->pos_start; i < passage_ptr->pos_end; i++)
            st[i][passage_ptr->pos_fix] = O_EMPTY;
    passage_ptr->init = 0;
}

/* Object constructor */
struct passage passage_init(enum passage_orientation ori, int pos_fix,
          int pos_start, int pos_end)
{
    struct passage passage;
    passage.o.update = _passage_update;
    passage.o.draw = _passage_draw;
    passage.o.destroy = _passage_destroy;
    passage.ori = ori;
    passage.pos_fix = pos_fix;
    passage.pos_start = pos_start;
    passage.pos_end = pos_end;
    return passage;
}
