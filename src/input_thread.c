/*This file need the option "-lpthread" to be compiled succesfully by gcc.*/

#include "input_thread.h"

/* Global static variables.
 * Variable character keep the value of the user's last input. It is used by the functions *input_reader
and get_input.
 * Variable thread_num is used to control number of created threads.*/
static char character;
static short int thread_num = 0;


/* This function reads the user input without the need for pressing line break ("enter")
after each input. This is achieved by chaning properties of the terminal i/o.
 * For more information, check "man stty" documentation.*/
void *input_reader(void* vargp)
{
    (void)vargp;
    system("/bin/stty -icanon -echo");
	while(character != DEATH_LETTER)
		character = getchar();

	system("/bin/stty icanon echo");

	return NULL;
}

/* This function creates thread responsible for reading inputs if it has not been yet created once.
 * It checks for the global static variable "thread_num" to check whether a thread has already been created.
 * Caller is still responsable for killing the thread at the end of execution by either calling 
pthread_kill(pthread_t thread, int sig) or by the used inputing DEATH_LETTER.
 * OUTPUT: id of the created thread.*/
pthread_t GO_input_thread()
{
	pthread_t thread;

	if (thread_num == 0)
	{
		pthread_create(&thread, NULL, input_reader,NULL);
		thread_num = 1;
	}
	else
	{
		printf("%sERROR: two input_threads created!\n",
               "\x1B[31m");
        exit(1);
	}

	return thread;
}

/* This function returns the last input given by the user and clears it.
 * OUTPUT: user's last input.*/
char get_input()
{
	char c = character;

	character = 0;
	return c;
}
