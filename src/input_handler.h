#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include "object.h"

enum players
{
    KNIGHT = 0,
    MAGE = 1,
    THIEF = 2,
    P_COUNT = 3
};

enum p_input
{
    IDLE,
    MOVE,
};

void
set_players(char knight, char mage, char thief);

int
get_player_input(t_stage st);


#endif
