#include "capihu_time.h"
#include <time.h>

static t_time _ms_zero;

static t_time
_time_get_ms(void)
{
    struct timespec t;
    timespec_get(&t, TIME_UTC);
    return (t.tv_nsec / 1000000) + (t.tv_sec * 1000);
}

void
time_init(void)
{
    _ms_zero = _time_get_ms();
}

t_time
time_get_ms(void)
{
    return _time_get_ms() - _ms_zero;
}
