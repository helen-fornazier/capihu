#ifndef STONE_OBJECTS_HEADER
#define STONE_OBJECTS_HEADER

#include <time.h>
#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define STONE_VISUAL '@'
#define STONE_COLOUR WHT
#define STONE_MOV_UPDATE_TIME 10 //the higher the value the slower the stone moves

/* Possible states for the stone.*/
enum stone_state
{
    STONE_UP,
    STONE_DOWN,
    STONE_LEFT,
    STONE_RIGHT,
    STONE_NOT_MOVING
};

/* stone objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update.
 * magic_state controls wheather the magic is moving and its direction.
 * already_moved_this_cycle is a flag to control its speed moviment.*/
struct stone
{
    struct object o;
    int pos_x, pos_y;
    char init;
    enum stone_state state;
    int already_moved_this_cycle;
    char grabbed;
};


struct stone stone_init(int pos_x, int pos_y);
void change_stone_state_and_grab_state(enum stone_state state,int grabbed, int pos_x, int pos_y);

#endif
