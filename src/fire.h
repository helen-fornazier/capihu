#ifndef FIRE_OBJECTS_HEADER
#define FIRE_OBJECTS_HEADER

#include <time.h>
#include "object.h"
#include "wall.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters. As fire is animated, VISUAL1 and VISUAL2 represent 
each animation frame. */
#define FIRE_VISUAL1 'S'
#define FIRE_VISUAL2 's'
#define VISUAL_CYCLE_TIME 30
#define FIRE_COLOUR RED

/* fire is like walls and come in rows or collumns. */
/*enum f_orientation
{
    HORIZONTAL,
    VERTICAL,
};*/

/* fire objects.
 * orientation determines if it is vertical or horizontal.
 * if vertical, pos_fix is the COLUMN and pos_start and end
 * are the start and end of the wall LINES.
 * if horizontal swap COLUMN for lines */
struct fire
{
    struct object o;
    enum w_orientation ori;
    int
        pos_fix,
        pos_start,
        pos_end;
    char init;
    char visual;
    int already_moved_this_cycle;
};

struct fire fire_init(enum w_orientation ori, int pos_fix,
          int pos_start, int pos_end);

#endif
