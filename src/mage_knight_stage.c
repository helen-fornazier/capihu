#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "input_handler.h"
#include "capihu_time.h"
#include "string.h"
#include "players.h"
#include "magic.h"
#include "door_button_floor.h"
#include "button_floor.h"
#include "door_button_wall.h"
#include "button_wall.h"
#include "fire.h"
#include "attack.h"
#include "bomb.h"

extern struct guard g_guard;
extern struct key g_key;

extern struct player g_knight;
extern struct player g_mage;
extern struct player g_thief;
extern struct magic g_magic;
extern struct door_button_wall g_door_button_wall;
extern struct button_wall g_button_wall;
extern struct attack g_attack;
extern struct guard g_guard;
extern struct bomb g_bomb;

int
main(void)
{
    struct wall w1, w2,w3;// w4;//,w5,w6,w7;
    struct fire f1, f2;
    struct object *array[14];
    t_stage st;
    struct screen s;
    char update;

    memset(st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    f1 = fire_init(HORIZONTAL, 5, 5, 47);
    f2 = fire_init(HORIZONTAL, 12, 5, 50);
    w1 = wall_init(VERTICAL, 5, 5, 17);
    w2 = wall_init(VERTICAL, 50, 5, 20);
    w3 = wall_init(HORIZONTAL, 20, 5, 50);

    g_door_button_wall = door_button_wall_init(48,5);
    g_button_wall = button_wall_init(9,3);
    mage_init(40, 16, st);
    g_magic = magic_init(3,3,MAGIC_NOT_EXISTING);
    guard_init(25, 12, DOWN, 0);
    knight_init(10, 10, st);
    attack_init();
    g_bomb = bomb_init(41,18);
    
    screen_set_dimension(&s, 23, 60);

    array[0] = &f1.o;
    array[1] = &f2.o;
    array[2] = &w1.o;
    array[3] = &w2.o;
    array[4] = &w3.o;
    array[5] = &g_mage.o;
    array[6] = &g_magic.o;
    array[7] = &g_door_button_wall.o;
    array[8] = &g_button_wall.o;
    array[9] = &g_knight.o;
    array[10]= &g_guard.o;
    array[11]= &g_attack.o;
    array[12]= &g_bomb.o;

    set_players(1, 1, 0);
    while(1)
    {
        update = 0;
        if (!get_player_input(st)) return 0;
        for (int i = 0; i < 13; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 13; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
        if (g_mage.pos_x <= 5)
            g_mage.pos_x = 5;
        if (g_knight.pos_y <= 5)
            g_knight.pos_y = 5;
        if (g_mage.pos_x <= 5 && g_knight.pos_y <= 5)
            return 0;
    }
}
