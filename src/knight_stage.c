#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "input_handler.h"
#include "capihu_time.h"
#include "string.h"
#include "players.h"
#include "magic.h"
#include "attack.h"
#include "door_key.h"

extern struct guard g_guard;
extern struct player g_knight;
extern struct attack g_attack;

int
main(void)
{
    struct wall w1, w2, w3, w4;
    struct object *array[7];
    t_stage st;
    struct screen s;
    char update;

    memset(st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    w1 = wall_init(HORIZONTAL, 6, 6, 30);
    w2 = wall_init(VERTICAL, 6, 6, 15);
    w3 = wall_init(HORIZONTAL, 14, 6, 30);
    w4 = wall_init(VERTICAL, 29, 6, 11);
    guard_init(25, 8, DOWN, 0);
    knight_init(10, 10, st);
    attack_init();

    screen_set_dimension(&s, 18, 32);

    array[0] = &w1.o;
    array[1] = &w2.o;
    array[2] = &w3.o;
    array[3] = &w4.o;
    array[4] = &g_guard.o;
    array[5] = &g_knight.o;
    array[6] = &g_attack.o;

    set_players(1, 0, 0);
    while(1)
    {
        update = 0;
        if (!get_player_input(st)) return 0;
        for (int i = 0; i < 7; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 7; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
        if ((g_knight.pos_x == 30) && (g_knight.pos_y == 12))
            return 0;
    }
}
