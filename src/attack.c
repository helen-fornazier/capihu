#include "attack.h"
#include <stdlib.h>

struct attack g_attack;

static int
_attack_update(struct object *o, t_stage st)
{
    t_time time;
    (void)o;
    if (!(g_attack.stop || g_attack.stun))
    {
        if (g_attack.vertical)
        {
            for (int i = -1; i < 2; i++)
                if (st[g_attack.pos_y + i][g_attack.pos_x] == O_GUARD)
                    guard_stun();
        }
        else
        {
            for (int i = -1; i < 2; i++)
                if (st[g_attack.pos_y][g_attack.pos_x + i] == O_GUARD)
                    guard_stun();
        }
        g_attack.stun = 1;
        return 1;
    }
    if (!g_attack.stop)
    {
        time = time_get_ms();
        if ((time - g_attack.start) > ATTACK_TIME)
        {
            g_attack.stop = 1;
            return 1;
        }
    }
    return 0;
}

static void
_attack_draw(struct object *o, struct screen *s)
{
    (void)o;
    if (!g_attack.stop)
    {
        if (g_attack.vertical)
        {
            for (int i = -1; i < 2; i++)
            {
                s->c[g_attack.pos_y + i][g_attack.pos_x]
                = ATTACK_COLOR;
                s->m[g_attack.pos_y + i][g_attack.pos_x]
                = ATTACK_VISUAL;
            }
        }
        else
        {
            for (int i = -1; i < 2; i++)
            {
                s->c[g_attack.pos_y][g_attack.pos_x + i]
                = ATTACK_COLOR;
                s->m[g_attack.pos_y][g_attack.pos_x + i]
                = ATTACK_VISUAL;
            }
        }
    }
}

void
attack_init(void)
{
    g_attack.stop = 1;
    g_attack.o.update = _attack_update;
    g_attack.o.draw = _attack_draw;
    g_attack.o.destroy = NULL;
}

void
attack(int pos_x, int pos_y, char vertical)
{
    g_attack.pos_x = pos_x;
    g_attack.pos_y = pos_y;
    g_attack.vertical = vertical;
    g_attack.start = time_get_ms();
    g_attack.stop = 0;
    g_attack.stun = 0;
}
