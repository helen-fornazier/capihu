#include "door_button_floor.h"

struct door_button_floor g_door_button_floor;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int
_door_button_floor_update(struct object *o, t_stage st)
{
    struct door_button_floor *door_button_floor_ptr = (struct door_button_floor*)o;

    if(door_button_floor_ptr->state == DOOR_BUTTON_FLOOR_OPEN)
    {
        for(int i = -1; i <= 1; i++)
            st[door_button_floor_ptr->pos_y][i + door_button_floor_ptr->pos_x] = O_EMPTY;
        if(door_button_floor_ptr->init == 0)
        {
            door_button_floor_ptr->init = 1;
            return 1;
        }
        else return 0;
    }
    else
    {
        for(int i = -1; i <= 1; i++)
            st[door_button_floor_ptr->pos_y][i + door_button_floor_ptr->pos_x] = O_DOOR_BUTTON_FLOOR;
        if(door_button_floor_ptr->init == 0)
        {
            door_button_floor_ptr->init = 1;
            return 1;
        }
        else return 0;
    }

}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void
_door_button_floor_draw(struct object *o, struct screen *s)
{
    struct door_button_floor *door_button_floor_ptr = (struct door_button_floor*)o;

    if(door_button_floor_ptr->state == DOOR_BUTTON_FLOOR_OPEN)
    {
        s->c[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x-1] = NRM;
        s->m[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x-1] = ' ';
        s->c[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x] = NRM;
        s->m[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x] = ' ';
        s->c[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x+1] = NRM;
        s->m[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x+1] = ' ';
        return;
    }
    
    s->c[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x-1] = DOOR_BUTTON_FLOOR_COLOUR1;
    s->m[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x-1] = DOOR_BUTTON_FLOOR_VISUAL1;
    s->c[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x] = DOOR_BUTTON_FLOOR_COLOUR2;
    s->m[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x] = DOOR_BUTTON_FLOOR_VISUAL2;
    s->c[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x+1] = DOOR_BUTTON_FLOOR_COLOUR1;
    s->m[door_button_floor_ptr->pos_y][door_button_floor_ptr->pos_x+1] = DOOR_BUTTON_FLOOR_VISUAL1;
}

/* This function erases the object *o from stage.*/
static void
_door_button_floor_destroy(struct object *o, t_stage st)
{
    struct door_button_floor *door_button_floor_ptr = (struct door_button_floor*)o;
    
    for(int i = 0; i < 3; i++)
    	st[door_button_floor_ptr->pos_y][i + door_button_floor_ptr->pos_x] = O_EMPTY;

    door_button_floor_ptr->init =0;
}

/* This function destroys the door and it is called by the hero when he/she presses 
the appropriate button_floor.*/
void door_button_floor_unlock()
{
    g_door_button_floor.state = DOOR_BUTTON_FLOOR_OPEN;
}

/* This function rebuilds the door and it is called by the hero when he/she stops pressing 
the appropriate button_floor.*/
void door_button_floor_relock()
{
    g_door_button_floor.state = DOOR_BUTTON_FLOOR_CLOSED;
}

/* Object constructor */
struct door_button_floor door_button_floor_init(int pos_x, int pos_y)
{
    struct door_button_floor d;
    d.o.update = _door_button_floor_update;
    d.o.draw = _door_button_floor_draw;
    d.o.destroy = _door_button_floor_destroy;
    d.pos_x = pos_x;
    d.pos_y = pos_y;
    d.init = 0;
    d.state = DOOR_BUTTON_FLOOR_CLOSED;
    return d;
}
