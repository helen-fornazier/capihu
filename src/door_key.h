#ifndef DOOR_KEY_OBJECTS_HEADER
#define DOOR_KEY_OBJECTS_HEADER

#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define DOOR_KEY_VISUAL1 '='
#define DOOR_KEY_VISUAL2 '}'
#define DOOR_KEY_COLOUR1 WHT
#define DOOR_KEY_COLOUR2 YEL

/* dor_key objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update. */
struct door_key
{
    struct object o;
    int pos_x,pos_y;
    char update, init, real;
};

void door_key_unlock();
struct door_key door_key_init(int pos_x, int pos_y);

#endif
