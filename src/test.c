#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "capihu_time.h"
#include "string.h"

extern struct guard g_guard;
extern struct key g_key;

int
main(void)
{
    struct wall w1, w2, w3, w4;
    struct object *array[6];
    t_stage st;
    struct screen s;
    char input;
    char update;

    memset(&st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    w1 = wall_init(HORIZONTAL, 6, 6, 30);
    w2 = wall_init(VERTICAL, 6, 6, 30);
    w3 = wall_init(HORIZONTAL, 29, 6, 30);
    w4 = wall_init(VERTICAL, 29, 6, 30);
    guard_init(10, 10, DOWN, 1);
    key_init();
    screen_set_dimension(&s, 35, 35);

    array[0] = &w1.o;
    array[1] = &w2.o;
    array[2] = &w3.o;
    array[3] = &w4.o;
    array[4] = &g_guard.o;
    array[5] = &g_key.o;

    while(1)
    {
        update = 0;
        input = get_input();
        if (input == 'l') return 0;
        if (input != 0) guard_stun();
        for (int i = 0; i < 6; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 6; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
    }
}
