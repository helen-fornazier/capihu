#ifndef DOOR_BUTTON_FLOOR_OBJECTS_HEADER
#define DOOR_BUTTON_FLOOR_OBJECTS_HEADER

#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define DOOR_BUTTON_FLOOR_VISUAL1 '='
#define DOOR_BUTTON_FLOOR_VISUAL2 '+'
#define DOOR_BUTTON_FLOOR_COLOUR1 WHT
#define DOOR_BUTTON_FLOOR_COLOUR2 RED

enum door_button_floor_state
{
    DOOR_BUTTON_FLOOR_OPEN,
    DOOR_BUTTON_FLOOR_CLOSED
};

/* door_button_floor objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update. */
struct door_button_floor
{
    struct object o;
    int pos_x,pos_y;
    char init;
    enum door_button_floor_state state;
};

void door_button_floor_unlock();
void door_button_floor_relock();
struct door_button_floor door_button_floor_init(int pos_x, int pos_y);

#endif
