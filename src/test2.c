#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "input_handler.h"
#include "capihu_time.h"
#include "string.h"
#include "players.h"
#include "magic.h"
#include "attack.h"
#include "door_key.h"

extern struct guard g_guard;
extern struct key g_key;

extern struct player g_knight;
extern struct player g_mage;
extern struct player g_thief;
extern struct magic g_magic;
extern struct attack g_attack;
extern struct door_key g_door_key;

int
main(void)
{
    struct wall w1, w2, w3, w4;
    struct object *array[10];
    t_stage st;
    struct screen s;
    char update;

    memset(st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    w1 = wall_init(HORIZONTAL, 6, 6, 80);
    w2 = wall_init(VERTICAL, 6, 6, 40);
    w3 = wall_init(HORIZONTAL, 39, 6, 80);
    w4 = wall_init(VERTICAL, 80, 6, 40);
    guard_init(20, 20, DOWN, 1);
    key_init();
    knight_init(10, 10, st);
    mage_init(50, 10, st);
    thief_init(10, 30, st);
    g_magic = magic_init(0,0,MAGIC_NOT_EXISTING);
    g_door_key = door_key_init(15, 15);
    attack_init();

    screen_set_dimension(&s, 50, 85);

    array[0] = &w1.o;
    array[1] = &w2.o;
    array[2] = &w3.o;
    array[3] = &w4.o;
    array[4] = &g_guard.o;
    array[5] = &g_thief.o;
    array[6] = &g_knight.o;
    array[7] = &g_mage.o;
    array[8] = &g_key.o;
    array[9] = &g_magic.o;
    array[10] = &g_attack.o;
    array[11] = &g_door_key.o;

    set_players(1, 1, 1);
    while(1)
    {
        update = 0;
        if (!get_player_input(st)) return 0;
        for (int i = 0; i < 12; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 12; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
    }
}
