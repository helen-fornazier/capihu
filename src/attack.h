#ifndef ATTACK_HEADER
#define ATTACK_HEADER

#include "object.h"
#include "guard.h"
#include "capihu_time.h"

struct attack
{
    struct object o;
    t_time start;
    int pos_x, pos_y;
    char stop, stun, vertical;
};

#define ATTACK_TIME 200
#define ATTACK_VISUAL 'X'
#define ATTACK_COLOR RED

void
attack_init(void);

void
attack(int pos_x, int pos_y, char vertical);

#endif
