/*This file need the option "-lpthread" to be compiled succesfully by gcc.*/

#include "audio_thread.h"

/* This function play the audio given by the string path vargp.
 * INPUT: audio file path. */
void *play_audio(void* vargp)
{
    //(char*)vargp;
    char str[50];
    
    strcpy(str,"play ");
    strcat(str, vargp);
    //strcat(str," >/dev/null");
    strcat(str," -q");
	system(str);

	return NULL;
}

/* This function creates thread responsible for playing an audio. Audio file path
should be given as argument *str.
 * INPUT: audio file path.*/
pthread_t PLAY_audio_thread(char *str)
{
	pthread_t thread;
	pthread_create(&thread, NULL, play_audio, str);
	
	return thread;
}
