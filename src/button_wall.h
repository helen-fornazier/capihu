#ifndef BUTTON_WALL_OBJECTS_HEADER
#define BUTTON_WALL_HEADER

#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define BUTTON_WALL_VISUAL 'O'
#define BUTTON_WALL_COLOUR CYN

enum button_wall_state
{
    BUTTON_WALL_OFF,
    BUTTON_WALL_ON
};

/* button_wall objects.
 * pos_x and pos_y for position.
 * init is a control flag for the function update. */
struct button_wall
{
    struct object o;
    int pos_x, pos_y;
    char init;
    enum button_wall_state state;
};

struct button_wall button_wall_init(int pos_x, int pos_y);
void button_wall_change_state(enum button_wall_state state);

#endif
