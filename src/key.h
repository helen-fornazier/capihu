#ifndef KEY_OBJECTS_HEADER
#define KEY_OBJECTS_HEADER

#include "object.h"

/* VISUAL values are maped as the "characters" that appears on screen, while COLOUR define the 
font style used for its respective  characters.*/
#define KEY_VISUAL '{'
#define KEY_COLOUR YEL

/* key objects.
 * pos_x and pos_y for position.
 * dropped and grabbed are attributes to control interation with thief. */
struct key
{
    struct object o;
    int pos_x, pos_y;
    char
        dropped,
        grabbed,
        updated;
};

void
key_init(void);

void
key_drop(int pos_x, int pos_y, t_stage st);

void
key_grab(t_stage st);

#endif
