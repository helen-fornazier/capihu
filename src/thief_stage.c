#include <stdio.h>

#include "screen.h"
#include "guard.h"
#include "wall.h"
#include "key.h"
#include "input_thread.h"
#include "input_handler.h"
#include "capihu_time.h"
#include "string.h"
#include "players.h"
#include "magic.h"
#include "attack.h"
#include "door_key.h"
#include "stone.h"
#include "button_wall.h"
#include "door_button_wall.h"

extern struct key g_key;
extern struct player g_thief;
extern struct door_key g_door_key;
extern struct stone g_stone;
extern struct button_wall g_button_wall;
extern struct door_button_wall g_door_button_wall;

int
main(void)
{
    struct wall w1, w2, w3, w4, w5;
    struct object *array[10];
    t_stage st;
    struct screen s;
    char update;

    memset(st, O_EMPTY, sizeof(st));
    time_init();
    GO_input_thread();

    w1 = wall_init(HORIZONTAL, 2, 2, 16);
    w2 = wall_init(VERTICAL, 2, 2, 16);
    w3 = wall_init(HORIZONTAL, 10, 6, 16);
    w4 = wall_init(VERTICAL, 15, 2, 10);
    w5 = wall_init(VERTICAL, 6, 10, 16);
    key_init();
    key_drop(10, 3, st);
    thief_init(6, 6, st);
    g_door_key = door_key_init(4, 10);
    g_stone = stone_init(4, 12);
    g_button_wall = button_wall_init(13, 7);
    g_door_button_wall = door_button_wall_init(4, 14);

    screen_set_dimension(&s, 18, 18);

    array[0] = &w1.o;
    array[1] = &w2.o;
    array[2] = &w3.o;
    array[3] = &w4.o;
    array[4] = &w5.o;
    array[5] = &g_thief.o;
    array[6] = &g_key.o;
    array[7] = &g_stone.o;
    array[8] = &g_door_button_wall.o;
    array[9] = &g_door_key.o;
    array[10] = &g_button_wall.o;

    set_players(0, 0, 1);
    while(1)
    {
        update = 0;
        if (!get_player_input(st)) return 0;
        for (int i = 0; i < 11; i++)
            update += array[i]->update(array[i], st);
        if (!update) continue;
        screen_clear(&s);
        for (int i = 0; i < 11; i++)
            array[i]->draw(array[i], &s);
        screen_draw(&s);
        if ((g_thief.pos_x == 4) && (g_thief.pos_y == 15))
            return 0;
    }
}
