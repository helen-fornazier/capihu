/* This file need the option "-lpthread" to be compiled succesfully by gcc.
 * Needs to install sox with libraries.
	sudo apt-get install sox
	sudo apt-get update
	sudo apt-get install libsox-dev
 * Definition of the declared functions o nthis file is found in "audio_thread.c".*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

/* This function play the audio given by the string path vargp.
 * INPUT: audio file path. */
void *play_audio(void* vargp);

/* This function creates thread responsible for playing an audio. Audio file path
should be given as argument *str.
 * INPUT: audio file path.*/
pthread_t PLAY_audio_thread(char *str);