#include "players.h"
#include "key.h"
#include "magic.h"
#include "stone.h"
#include "attack.h"

#define KNIGHT_VISUAL 'H'
#define KNIGHT_SELECT BLINKING_B_BLU
#define KNIGHT_DEFAULT B_BLU

#define MAGE_VISUAL 'C'
#define MAGE_SELECT BLINKING_B_RED
#define MAGE_DEFAULT B_RED

#define THIEF_VISUAL 'P'
#define THIEF_SELECT BLINKING_B_MAG
#define THIEF_DEFAULT B_MAG

struct player g_knight;
struct player g_mage;
struct player g_thief;

extern struct key g_key;
extern struct magic g_magic;
extern struct stone g_stone;

void
knight_special(t_stage st)
{
    (void)st;
    int mult = (g_knight.dir == LEFT || g_knight.dir == UP ? -1 : 1);
    if (g_knight.dir == LEFT || g_knight.dir == RIGHT)
        attack(g_knight.pos_x + (2 * mult), g_knight.pos_y, 1);
    else
        attack(g_knight.pos_x, g_knight.pos_y + (2 * mult), 0);
}

void
mage_special(t_stage st)
{
    //(void)st;
    st[g_magic.pos_y][g_magic.pos_x] = O_EMPTY;
    if(g_mage.dir == UP)
        change_magic_state_and_grab_state(MAGIC_UP,0, g_mage.pos_x, g_mage.pos_y-2);
    if(g_mage.dir == DOWN)
        change_magic_state_and_grab_state(MAGIC_DOWN,0, g_mage.pos_x, g_mage.pos_y+2);
    if(g_mage.dir == LEFT)
        change_magic_state_and_grab_state(MAGIC_LEFT,0, g_mage.pos_x-2, g_mage.pos_y);
    if(g_mage.dir == RIGHT)
        change_magic_state_and_grab_state(MAGIC_RIGHT,0, g_mage.pos_x+2, g_mage.pos_y);
}

void
thief_special(t_stage st)
{
    if (g_key.grabbed)
    {
        int mult = ((g_thief.dir == UP) || (g_thief.dir == LEFT)
                    ? -1 : 1);
        if ((g_thief.dir == LEFT) || g_thief.dir == RIGHT)
            key_drop(g_thief.pos_x + (mult * 2), g_thief.pos_y, st);
        else
            key_drop(g_thief.pos_x, g_thief.pos_y + (mult * 2), st);
        return;
    }
    else if (g_stone.grabbed)
    {
        st[g_stone.pos_y][g_stone.pos_x] = O_EMPTY;
        if(g_thief.dir == UP)
            change_stone_state_and_grab_state(STONE_UP,0, g_thief.pos_x, g_thief.pos_y-2);
        if(g_thief.dir == DOWN)
            change_stone_state_and_grab_state(STONE_DOWN,0, g_thief.pos_x, g_thief.pos_y+2);
        if(g_thief.dir == LEFT)
            change_stone_state_and_grab_state(STONE_LEFT,0, g_thief.pos_x-2, g_thief.pos_y);
        if(g_thief.dir == RIGHT)
            change_stone_state_and_grab_state(STONE_RIGHT,0, g_thief.pos_x+2, g_thief.pos_y);
    }
    

}

static void
_player_stage(struct player *p, t_stage st)
{
    for (int i = -1; i < 2; i++)
        for (int j = -1; j < 2; j++)
            st[p->pos_y + i][p->pos_x + j] = p->id;
}

static void
_player_print(struct player *p, struct screen *s)
{
    enum colour c = (p->selected ? p->sc : p->c);
    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            s->c[p->pos_y + i][p->pos_x + j] = p->c;
            s->m[p->pos_y + i][p->pos_x + j] = p->letter;
        }
    }
    s->c[p->pos_y - 1][p->pos_x - 1] = c;
    s->m[p->pos_y - 1][p->pos_x - 1] = p->letter;
}

static enum obj_id
_player_move(struct player *p, t_stage st)
{
    int mult;
    enum obj_id id;

    if ((p->dir == UP) || (p->dir == LEFT)) mult = -1;
    else mult = 1;
    if ((p->dir == LEFT) || (p->dir == RIGHT))
    {
        for (int i = -1; i < 2; i++)
        {
            id = st[p->pos_y + i][p->pos_x + (mult * 2)];
            if (id != O_EMPTY)
                return id;
        }
        for (int i = -1; i < 2; i++)
        {
            st[p->pos_y + i][p->pos_x + (mult * -1)] = O_EMPTY;
            st[p->pos_y + i][p->pos_x + (mult * 2)] = p->id;
        }
        p->pos_x += 1 * mult;
    }
    else
    {
        for (int i = -1; i < 2; i++)
        {
            id = st[p->pos_y + (mult * 2)][p->pos_x + i];
            if (id != O_EMPTY)
                return id;
        }
        for (int i = -1; i < 2; i++)
        {
            st[p->pos_y + (mult * -1)][p->pos_x + i] = O_EMPTY;
            st[p->pos_y + (mult * 2)][p->pos_x + i] = p->id;
        }
        p->pos_y += 1 * mult;
    }
    return O_EMPTY;
}

static int
_player_update(struct object *o, t_stage st)
{
    struct player *p = (struct player*)o;
    enum obj_id colision;
    if (p->selected != p->previous)
    {
        p->previous = p->selected;
        return 1;
    }
    if (p->input == IDLE) return 0;
    colision = _player_move(p, st);
    p->input = IDLE;
    if (colision == O_EMPTY) return 1;
    if ((colision == O_KEY) && (p == &g_thief))
    {
        st[g_key.pos_y][g_key.pos_x] = O_EMPTY;
        g_key.grabbed = 1;
    }
    if ((colision == O_STONE) && (p == &g_thief))
    {
        st[g_stone.pos_y][g_stone.pos_x] = O_EMPTY;
        g_stone.grabbed = 1;
        g_stone.init = 0;
    }
    /* TODO: handle colisions */
    return 1;
}

static void
_player_draw(struct object *o, struct screen *s)
{
    struct player *p = (struct player*)o;
    _player_print(p, s);
}

static void
_player_destroy(struct object *o, t_stage st)
{
    (void)o;
    (void)st;
    /* TODO */
}

void
knight_init(int pos_x, int pos_y, t_stage st)
{
    g_knight.o.update = _player_update;
    g_knight.o.draw = _player_draw;
    g_knight.o.destroy = _player_destroy;
    g_knight.pos_x = pos_x;
    g_knight.pos_y = pos_y;
    g_knight.selected = 0;
    g_knight.previous = 0;
    g_knight.letter = KNIGHT_VISUAL;
    g_knight.dir = UP;
    g_knight.id = O_KNIGHT;
    g_knight.c = KNIGHT_DEFAULT;
    g_knight.sc = KNIGHT_SELECT;
    g_knight.input = MOVE;
    _player_stage(&g_knight, st);
}

void
mage_init(int pos_x, int pos_y, t_stage st)
{
    g_mage.o.update = _player_update;
    g_mage.o.draw = _player_draw;
    g_mage.o.destroy = _player_destroy;
    g_mage.pos_x = pos_x;
    g_mage.pos_y = pos_y;
    g_mage.selected = 0;
    g_mage.previous = 0;
    g_mage.letter = MAGE_VISUAL;
    g_mage.dir = UP;
    g_mage.id = O_MAGE;
    g_mage.c = MAGE_DEFAULT;
    g_mage.sc = MAGE_SELECT;
    g_mage.input = MOVE;
    _player_stage(&g_mage, st);
}

void
thief_init(int pos_x, int pos_y, t_stage st)
{
    g_thief.o.update = _player_update;
    g_thief.o.draw = _player_draw;
    g_thief.o.destroy = _player_destroy;
    g_thief.pos_x = pos_x;
    g_thief.pos_y = pos_y;
    g_thief.selected = 0;
    g_thief.previous = 0;
    g_thief.letter = THIEF_VISUAL;
    g_thief.dir = UP;
    g_thief.id = O_THIEF;
    g_thief.c = THIEF_DEFAULT;
    g_thief.sc = THIEF_SELECT;
    g_thief.input = MOVE;
    _player_stage(&g_thief, st);
}
