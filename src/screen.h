#ifndef SCREEN_HEADER
#define SCREEN_HEADER

/* This object will represent the game screen.
 * It works as if every pixel is the equivalent
 * of an ASCII character  */

/* define the maximum dimensions of the screen */
#define S_LINE 70
#define S_COLUMN 140

enum colour
{
    NRM,
    BLK,
    RED,
    GRN,
    YEL,
    BLU,
    MAG,
    CYN,
    WHT,
    B_BLK,
    B_RED,
    B_GRN,
    B_YEL,
    B_BLU,
    B_MAG,
    B_CYN,
    B_WHT,
    GRY,
    BLINKING_B_RED,
    BLINKING_B_GRN,
    BLINKING_B_YEL,
    BLINKING_B_BLU,
    BLINKING_B_MAG,
    BLINKING_B_CYN,
    BLINKING_B_WHT,
};

struct screen
{
    int
        lin, // used lines
        col; // used columns
    char m[S_LINE][S_COLUMN]; // character matrix
    enum colour c[S_LINE][S_COLUMN]; // colour matrix
};

/* draw the screen */
void
screen_draw(struct screen *s);

/* set all colours to normal and all spaces to ' ' */
void
screen_clear(struct screen *s);

/* set the screen dimensions, crash the program if
 * dimensions are bigger then the expected */
void
screen_set_dimension(struct screen *s, int l, int c);

#endif
