#ifndef CAPIHU_TIME
#define CAPIHU_TIME

typedef unsigned long int t_time;

void
time_init(void);

t_time
time_get_ms(void);

#endif
