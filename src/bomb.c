#include "bomb.h"

struct bomb g_bomb;

/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.*/
static int
_bomb_update(struct object *o, t_stage st)
{
    struct bomb *bomb_ptr = (struct bomb*)o;
    if (bomb_ptr->init) return 0;
    for(int i = 0; i < 3; i++)
	    	st[bomb_ptr->pos_y][i + bomb_ptr->pos_x] = O_BOMB;
    bomb_ptr->init = 1;
    return 1;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void
_bomb_draw(struct object *o, struct screen *s)
{
    struct bomb *bomb_ptr = (struct bomb*)o;
    
    s->c[bomb_ptr->pos_y][bomb_ptr->pos_x + 0] = BOMB_COLOUR1;
    s->m[bomb_ptr->pos_y][bomb_ptr->pos_x + 0] = BOMB_VISUAL1;
    s->c[bomb_ptr->pos_y][bomb_ptr->pos_x + 1] = BOMB_COLOUR2;
    s->m[bomb_ptr->pos_y][bomb_ptr->pos_x + 1] = BOMB_VISUAL2;
    s->c[bomb_ptr->pos_y][bomb_ptr->pos_x + 2] = BOMB_COLOUR3;
    s->m[bomb_ptr->pos_y][bomb_ptr->pos_x + 2] = BOMB_VISUAL3;
}

/* This function erases the object *o from stage.*/
static void
_bomb_destroy(struct object *o, t_stage st)
{
    struct bomb *bomb_ptr = (struct bomb*)o;
    if (!bomb_ptr->init) return;
    for(int i = 0; i < 3; i++)
	    st[bomb_ptr->pos_y][i + bomb_ptr->pos_x] = O_EMPTY;
    bomb_ptr->init = 0;
}

/* Object constructor */
struct bomb bomb_init(int pos_x, int pos_y)
{
    struct bomb b;
    b.o.update = _bomb_update;
    b.o.draw = _bomb_draw;
    b.o.destroy = _bomb_destroy;
    b.pos_x = pos_x;
    b.pos_y = pos_y;
    return b;
}
