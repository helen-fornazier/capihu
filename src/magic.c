#include "magic.h"
#include "button_wall.h"

struct magic g_magic;


/* Function update decides whether the object *o is to be placed on the stage.
When the objet.init == 0, the object is not drawn by the draw function or is removed from the stage.
 * Time is used to control the speed of the moving magic and the control attribute controls 
 whether the magic has already moved in a given "tik cycle".*/
static int
_magic_update(struct object *o, t_stage st)
{
    struct magic *magic_ptr = (struct magic*)o;
    int horizontal = 0, vertical = 0;
    struct timespec time;
    long ntime;
    long diff;

    if(magic_ptr->grabbed == 1)
    {
        //stone_ptr->pos_x = g_thief.pos_x+1;
        //stone_ptr->pos_y = g_thief.pos_y;
        return 0;
    }

    if(magic_ptr->state == MAGIC_NOT_EXISTING)  return 0;
    if(magic_ptr->state == MAGIC_UP)     vertical = -1;
    if(magic_ptr->state == MAGIC_DOWN)   vertical = +1;
    if(magic_ptr->state == MAGIC_LEFT)   horizontal = -1;
    if(magic_ptr->state == MAGIC_RIGHT)  horizontal = +1;
    
    timespec_get(&time, TIME_UTC); //gets time in nanoses
    ntime = time.tv_nsec / 10000000;
    diff = ntime % (MAGIC_MOV_UPDATE_TIME);

    if(diff == 0 && magic_ptr->already_moved_this_cycle == 0)
    {
        magic_ptr->already_moved_this_cycle = 1;
        if(st[magic_ptr->pos_y+vertical][magic_ptr->pos_x+horizontal] == O_EMPTY || st[magic_ptr->pos_y+vertical][magic_ptr->pos_x+horizontal] == O_FIRE || st[magic_ptr->pos_y+vertical][magic_ptr->pos_x+horizontal] == O_BUTTON_FLOOR)
        {
            st[magic_ptr->pos_y][magic_ptr->pos_x] = O_EMPTY;
            magic_ptr->pos_y = magic_ptr->pos_y + vertical;
            magic_ptr->pos_x = magic_ptr->pos_x + horizontal;
            st[magic_ptr->pos_y][magic_ptr->pos_x] = O_MAGIC;
            return 1;
        }
        else if(st[magic_ptr->pos_y+vertical][magic_ptr->pos_x+horizontal] == O_BUTTON_WALL)
        {
            button_wall_change_state(BUTTON_WALL_ON);
            st[magic_ptr->pos_y][magic_ptr->pos_x] = O_EMPTY;
            magic_ptr->pos_y = magic_ptr->pos_y + vertical;
            magic_ptr->pos_x = magic_ptr->pos_x + horizontal;
            magic_ptr->state = MAGIC_NOT_EXISTING;
            return 1;
        }
        else
        {
            magic_ptr->state = MAGIC_NOT_EXISTING;
            st[magic_ptr->pos_y][magic_ptr->pos_x] = O_EMPTY;
            return 1;
        }
    }
    if(diff > 0)
        magic_ptr->already_moved_this_cycle = 0;
    return 0;
}

/* This function draws the object *o on the color matrix c and on the character matrix m.*/
static void _magic_draw(struct object *o, struct screen *s)
{
    struct magic *magic_ptr = (struct magic*)o;
    
    if(magic_ptr->state == MAGIC_NOT_EXISTING) return;

    s->c[magic_ptr->pos_y][magic_ptr->pos_x] = MAGIC_COLOUR;
    s->m[magic_ptr->pos_y][magic_ptr->pos_x] = MAGIC_VISUAL;
    
}

/* This function erases the object *o from stage.*/
static void _magic_destroy(struct object *o, t_stage st)
{
    struct magic *magic_ptr = (struct magic*)o;
    if (!magic_ptr->init) return;
        st[magic_ptr->pos_y][magic_ptr->pos_x] = O_EMPTY;
    magic_ptr->init = 0;
}

/* This function is called by the mage when it summon the magic or by the thief when he/she throws it.
It sets the direction and initial position of the magic.*/
void change_magic_state_and_grab_state(enum magic_state state,int grabbed, int pos_x, int pos_y)
{
    
    g_magic.state = state;
    g_magic.grabbed = grabbed;
    g_magic.pos_x = pos_x;
    g_magic.pos_y = pos_y;
}

/* Object constructor */
struct magic magic_init(int pos_x, int pos_y, enum magic_state state)
{
    struct magic m;
    m.o.update = _magic_update;
    m.o.draw = _magic_draw;
    m.o.destroy = _magic_destroy;
    m.pos_x = pos_x;
    m.pos_y = pos_y;
    m.state = state;
    m.already_moved_this_cycle = 0;
    m.grabbed = 0;
    return m;
}
